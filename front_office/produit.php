<?php require 'header.php'; ?>
<?php
$single = get_single( $_GET['id'] );
$url = $_GET['url'];
//  if($single['url'] != $_GET['url']){
// 	header("location:/akanjo/produit/".$single['url']."-".$single['id']);
// }
?>
<div class="shop_top">
<div class="container">

	<div class="row">
	<?php foreach($single as $single) { ?>
		<div class="col-md-9 single_left">
		   <div class="single_image">
			     <ul id="etalage">
					<li>
						<a href="optionallink.html">
						 	<img class="etalage_thumb_image" src="/akanjo/images/<?php echo $single->image; ?>.jpg" />  
							<!-- <img class="etalage_source_image" src="images/3.jpg" />  -->
						</a> 
					</li>
					
					
				</ul>
			    </div>
		        <!-- end product_slider -->
		        <div class="single_right">
		        	<h1><?php echo $single->nomProduit; ?> </h1>
		        	<p class="m_10"><?php echo $single->description; ?></p>
		        	
					
					
		        </div>
		        <div class="clear"> </div>
		</div>
		<div class="col-md-3">
		  <div class="box-info-product">
			<p class="price2">$<?php echo number_format ($single->prix,2,',',''); ?></p>

					<a href ="#" type="submit" name="Submit" class="exclusive">
					   <span>Add to cart</span>
					</a>
		   </div>
	   </div>
	</div>
	<?php } ?>		
	
		
 </div>
</div>
	  
<?php require'footer.php'; ?>