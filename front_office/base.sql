create database akanjo;

use akanjo;

create table user(
id int primary key AUTO_INCREMENT,
pseudo varchar(20),
mdp varchar(20),
email varchar(30)
);

create table categorie(
id int primary key AUTO_INCREMENT,
nomCat varchar(20)
);

create table sous_categorie(
id int primary key AUTO_INCREMENT,
idCat int, 
nomSousCat varchar(30)   
);

alter table sous_categorie add foreign key (idcat) references categorie(id);


create table produit(
id int primary key AUTO_INCREMENT,
idSCategorie int,
nomProduit varchar(20),
image varchar(20),
prix numeric,
quantite int    
);

alter table produit add foreign key (idscategorie) references sous_categorie(id);


-- categorie
insert into categorie(nomcat) values ('Homme');
insert into categorie(nomcat) values ('Femme');
insert into categorie(nomcat) values ('Enfant');

-- sous_categorie
insert into sous_categorie(idcat, nomSousCat) values (1,'Haut');
insert into sous_categorie(idcat, nomSousCat) values (1,'Sous_etement');
insert into sous_categorie(idcat, nomSousCat) values (1,'Bas');

insert into sous_categorie(idcat, nomSousCat) values (2,'Haut');
insert into sous_categorie(idcat, nomSousCat) values (2,'Sous_vetement');
insert into sous_categorie(idcat, nomSousCat) values (2,'Bas');

insert into sous_categorie(idcat, nomSousCat) values (3,'Haut');
insert into sous_categorie(idcat, nomSousCat) values (3,'Bas');

-- produit
insert into produit(idSCategorie,nomProduit,image,prix,quantite) values (3,'BlueJeans','',200,20);
insert into produit(idSCategorie,nomProduit,image,prix,quantite) values (5,'StringRouge','',20,100);
insert into produit(idSCategorie,nomProduit,image,prix,quantite) values (8,'ShortRouge','',10,20);






select produit.id,nomproduit,nomSousCat,nomCat,description,quantite,prix from sous_categorie,categorie,produit where produit.idSCategorie=sous_categorie.id and sous_categorie.idCat=categorie.id;


insert into user(pseudo,mdp) values ('test','test');
insert into user(pseudo,mdp) values ('admin','admin');