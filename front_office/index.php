<?php require 'header.php';?>

<?php
$produit = get_Produit();
// var_dump($produit);
?>

<div class="shop_top">
<div class="container">
	<div class="r/hop_box-top">
		<?php foreach($produit as $produit) { ?>
		<div class="col-md-3 shop_box"><a href="produit/<?php echo $produit->url; ?>-<?php echo $produit->id; ?>">
			<img src="/akanjo/images/<?php echo $produit->image;?>.jpg" class="img-responsive" alt=""/>
			<div class="shop_desc">
				<h3><a href="produit/<?php echo $produit->url; ?>-<?php echo $produit->id; ?>"><?php echo $produit->nomProduit; ?></a></h3>
				<span class="actual">$<?php echo number_format ($produit->prix,2,',',''); ?></span><br>
				<ul class="buttons">
					<li class="cart"><a href="#">Add To Cart</a></li>
					<div class="clear"> </div>
			    </ul>
		    </div>
		</a></div>
		<?php } ?>
	</div>
	
 </div>
</div>

<?php require 'footer.php'; ?>
